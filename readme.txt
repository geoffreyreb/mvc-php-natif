Bonjour, voici comment voir le site :

1) Installez le programme "Docker Desktop", ainsi qu'une invite de commande en langage "bash"
2) Allez dans le dossier "php-container" et lancez l'invite de commande depuis ce dossier (si l'option contextuelle n'apparait pas en faisant un clic droit sur le dossier,
    copiez le chemin du dossier (exemple : C:\Users\admin\Desktop\projet-gfv\php-container), allez dans votre invite de commande, écrivez "cd " et collez le chemin )
3) Lancez les commandes suivantes dans l'ordre :    docker-compose build
                                                    docker-compose up -d
                                                    docker ps
4) La commande docker ps devrait renvoyer quatre lignes, copiez le premier "container id" (sur la ligne "php"), et entrez les commandes suivantes dans l'ordre :
                                    winpty docker exec -it [le container id que vous avez copié] bash
                                    composer install
                                    composer update
                                    composer dump-autoload
                                    exit
5) Ouvrez une page internet à l'adresse "localhost:8809"
6) Créez un compte
7) Retournez sur votre invite de commande, copiez le deuxième "container id" (sur la ligne "mysql"), et entrez la commande suivante :
                                    winpty docker exec -it [le container id que vous avez copié] bash
9) Ouvrez le fichier "creabase.sql" situé dans "php-container/infra/mysql/dump" avec un éditeur de texte comme le bloc-notes, et copiez l'intégralité du contenu du fichier
10) Retournez sur votre invite de commande et entrez les commandes suivantes dans l'ordre (les points virgules sont importants):
                                    mysql -u root -proot
                                    [le code sql que vous avez copié]
                                    UPDATE Role_by_user SET id_role = 1 WHERE id_user = 1;
                                    exit
                                    exit
11) Le compte que vous avez créé devrait normalement être désormais administrateur ! Actualisez la page internet et passez votre souris sur
    votre nom d'utilisateur : si une petite fenêtre apparait avec le texte "Cliquez ici pour administrer le site",
    vous pouvez cliquer pour ajouter des vidéos, des rôles, des chaînes youtube ou modifier le rôle d'un utilisateur.
12) Pour ajouter une vidéo, vous devez d'abord ajouter une chaîne youtube.
                                                    