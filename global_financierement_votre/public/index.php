<?php

use App\DAO\UserDAO;
use App\Model\User;
use App\Utilities\AccessException;
use App\Utilities\Security;
use App\Utilities\MyException;
use App\Utilities\MyLogger;

require_once(realpath("../vendor/autoload.php"));
session_start();
// Security::verifyAuthenticationCookie();


function root($page){
    $pageContent = "../src/Controller/" . ucfirst($page) . "Controller.php";
    if (!file_exists($pageContent)) {
        $logger = new MyLogger("404");
        $logger->logInfo("Tying to access to : /".$page);
        $pageContent = "../src/Controller/ErrorController.php";
    }

    require_once(realpath($pageContent));
    try{
        index();
    }
    catch(AccessException $ae){
        $logger = new MyLogger("401");
        $user = isset($_SESSION['user']) ? $_SESSION['user']->getUserEmail() : "unknown user";
        $logger->logInfo("Tying to access to : /".$page . " by " . $user);
        header("location:../error?error=401");
        exit();
    }catch (MyException $e){
        $logger = new MyLogger("error");
        $logger->logInfo($e->getMessage());
        header("location:../");
        exit();
    }
}
if (isset($_GET['page'])) {
    root($_GET['page']);
} else {
    root('accueil');
}
