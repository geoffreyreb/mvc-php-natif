<?php

namespace App\DAO;
use App\Model\Article;
use App\Utilities\MyFunctions;

class ArticleDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM Article";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $articles = $statement->fetchAll();

            foreach ($articles as $article){
                $result = new Article($article["id_article"],
                                    $article["article_title"],
                                    $article["article_content"],
                                    $article["article_summary"],
                                    $article["article_image_path"],
                                    $article["article_image_description"],
                                    $article["article_date"],
                                    $article["id_user"]
                                    );
                Article::getArticleList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM Article WHERE :column = :value";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":column", $column);
            $statement->bindParam(":value", $value);
            $statement->execute();

            $article = $statement->fetch();
            if (!$article){
                // TODO //
            }

            $result = new Article($article["id_article"],
                                    $article["article_title"],
                                    $article["article_content"],
                                    $article["article_summary"],
                                    $article["article_image_path"],
                                    $article["article_image_description"],
                                    $article["article_date"],
                                    $article["id_user"]
                                    );
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $result;
    }

    public function create(object $object){
        $sql = "INSERT INTO Article   (article_title,
                                    article_content,
                                    article_summary,
                                    article_image_path,
                                    article_image_description,
                                    article_date,
                                    id_user) VALUES
                                    (:title,
                                    :content,
                                    :summary,
                                    :image_path,
                                    :image_description
                                    :date,
                                    :id_user)";
        try{
            $statement = $this->connexion->prepare($sql);
            MyFunctions::bindAllArticleParams($object, $statement);
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            Article::getArticleList()->offsetSet($object->getId(), $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function update(object $object){
        $sql = "UPDATE Article SET  article_title = :title,
                                    article_content = :content,
                                    article_summary = :summary,
                                    article_image_path = :image_path,
                                    article_image_description = :image_description,
                                    article_date = :date,
                                    id_user = :id_user
                            WHERE (id_article = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            MyFunctions::bindAllArticleParams($object, $statement);
            $statement->execute();
            Article::getArticleList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function delete(object $object){
        $sql = "DELETE FROM Article WHERE (id_article = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            Article::getArticleList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}
