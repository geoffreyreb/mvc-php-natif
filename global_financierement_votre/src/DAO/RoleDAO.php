<?php

namespace App\DAO;
use App\Model\Role;
use App\Utilities\MyFunctions;

class RoleDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM Role";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $roles = $statement->fetchAll();

            foreach ($roles as $role){
                $result = $this->instanceRole($role);
                Role::getRoleList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM Role WHERE " . $column . " = "  . $value ;
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();

            $role = $statement->fetch();
            if (!$role){
                // TODO //
            }

            $result = $this->instanceRole($role);
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $result;
    }

    public function findByUser(int $id_user){
        $sql = "SELECT * FROM Role
                INNER JOIN Role_by_user
                ON Role_by_user.id_role = Role.id_role
                INNER JOIN User
                ON User.id_user = Role_by_user.id_user
                WHERE User.id_user = :id";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":id", $id_user);
            $statement->execute();

            $roles = $statement->fetchAll();
            foreach ($roles as $role){
                $result = new Role($role["id_role"],
                                    $role["role_label"]
                                    );
                Role::getRoleList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function create(object $object){
        $sql = "INSERT INTO Role (role_label) VALUE (:label)";
        try{
            $statement = $this->connexion->prepare($sql);
            $label = $object->getRole_label();
            $statement->bindParam(":label", $label);
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            Role::getRoleList()->offsetSet($object->getId(), $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function update(object $object){
        $sql = "UPDATE Role SET   role_label = :role_label
                                    WHERE (id_role = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            $statement->bindParam(":label", $object->getLabel());
            $statement->execute();
            Role::getRoleList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function update_by_user(object $user, object $role){
        $sql = "UPDATE Role_by_user SET id_role = :id_role
                                        WHERE (id_user = :id_user)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id_user = $user->getId();
            $id_role = $role->getId();
            $statement->bindParam(":id_user", $id_user);
            $statement->bindParam(":id_role", $id_role);
            $statement->execute();
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function delete(object $object){
        $sql = "DELETE FROM Role WHERE (id_role = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            Role::getRoleList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    private function instanceRole($role){
        try{
            return new Role(
                $role["id_role"],
                $role["role_label"],
            );
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}