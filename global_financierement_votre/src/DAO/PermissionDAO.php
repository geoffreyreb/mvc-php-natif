<?php

namespace App\DAO;
use App\Model\Permission;
use App\Utilities\MyFunctions;

class PermissionDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM Permission";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $permissions = $statement->fetchAll();

            foreach ($permissions as $permission){
                $result = new Permission($permission["id_permission"],
                                    $permission["permission_label"]
                                    );
                Permission::getPermissionList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM Permission WHERE :column = :value";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":column", $column);
            $statement->bindParam(":value", $value);
            $statement->execute();

            $permission = $statement->fetch();
            if (!$permission){
                // TODO //
            }

            $result = new Permission($permission["id_permission"],
                                    $permission["permission_label"]
                                    );
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $result;
    }

    public function findByRole(string $role_label){
        $sql = "SELECT * FROM Permission
                INNER JOIN ACL
                ON ACL.id_permission = Permission.id_permission
                INNER JOIN Role
                ON ACL.id_role = Role.id_role
                WHERE Role.role_label = :role";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":role", $role_label);
            $statement->execute();

            $permissions = $statement->fetchAll();
            foreach ($permissions as $permission){
                $result = new Permission($permission["id_permission"],
                                    $permission["permission_label"]
                                    );
                Permission::getPermissionList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findByUser(int $id_user){
        $sql = "SELECT * FROM Permission
                INNER JOIN ACL
                ON ACL.id_permission = Permission.id_permission
                INNER JOIN Role_by_user
                ON Role_by_user.id_role = ACL.id_role
                INNER JOIN User
                ON User.id_user = Role_by_user.id_user
                WHERE User.id_user = :id";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":id", $id_user);
            $statement->execute();

            $permissions = $statement->fetchAll();
            foreach ($permissions as $permission){
                $result = new Permission($permission["id_permission"],
                                    $permission["permission_label"]
                                    );
                Permission::getPermissionList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function create(object $object){
        $sql = "INSERT INTO Permission (permission_label) VALUE
                                    (:label)";
        try{
            $statement = $this->connexion->prepare($sql);
            $label = $object->getLabel();
            $statement->bindParam(":label", $label);
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            Permission::getPermissionList()->offsetSet($object->getId(), $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function create_by_role(object $permission, object $role){
        $sql = "INSERT INTO ACL (id_permission, id_role) VALUES (:id_permission, :id_role)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id_permission = $permission->getId();
            $id_role = $role->getId();
            $statement->bindParam(":id_permission", $id_permission);
            $statement->bindParam(":id_role", $id_role);
            $statement->execute();
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            echo $t->getMessage();
        }
    }

    public function update(object $object){
        $sql = "UPDATE Permission SET  permission_label = :label
                            WHERE (id_permission = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            $statement->bindParam(":label", $object->getLabel());
            $statement->execute();
            Permission::getPermissionList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function delete(object $object){
        $sql = "DELETE FROM Permission WHERE (id_permission = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            Permission::getPermissionList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}
