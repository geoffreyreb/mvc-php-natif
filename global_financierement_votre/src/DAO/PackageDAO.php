<?php

namespace App\DAO;
use App\Model\Package;

class PackageDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM Package";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $packages = $statement->fetchAll();

            foreach ($packages as $package){
                $result = new Package($package["id_package"],
                                        $package["package_name"],
                                        $package["id_role"],
                                        );
                Package::getPackageList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM Package WHERE :column = :value";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":column", $column);
            $statement->bindParam(":value", $value);
            $statement->execute();

            $package = $statement->fetch();
            if (!$package){
                // TODO //
            }

            $result = new Package($package["id_package"],
                                $package["package_name"],
                                $package["id_role"]
                            );
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $result;
    }

    public function create(object $object){
        $sql = "INSERT INTO Package   (Package_name,
                                    id_role) VALUES
                                    (:name,
                                    :id_role)";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(':name', $object->getName());
            $statement->bindParam(':id_role', $object->getId_role());
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            Package::getPackageList()->offsetSet($object->getId(), $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function update(object $object){
        $sql = "UPDATE Package SET    package_name = :name,
                                    id_role = :id_role
                            WHERE (id_package = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            $statement->bindParam(':name', $object->getName());
            $statement->bindParam(':id_role', $object->getId_role());
            $statement->execute();
            Package::getPackageList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function delete(object $object){
        $sql = "DELETE FROM Package WHERE (id_package = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            Package::getPackageList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}
