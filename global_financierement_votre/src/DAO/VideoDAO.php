<?php

namespace App\DAO;
use App\Model\Video;
use App\Utilities\MyFunctions;
use App\Utilities\MyException;

class VideoDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM Video ORDER BY video_upload_date ASC";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $videos = $statement->fetchAll();

            foreach ($videos as $video){
                $result = new Video($video["id_video"],
                                    $video["video_title"],
                                    $video["video_link"],
                                    new \Datetime($video["video_upload_date"]),
                                    $video["id_channel"]
                                    );
                Video::getVideoList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM Video WHERE " . $column ." = " . $value;
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            
            $video = $statement->fetch();
            if (!$video){
                throw new MyException(
                    "Impossible de trouver la vidéo",
                    "video not found with column : " . $column . " and value : " . $value);
            }
                
            $result = new Video($video["id_video"],
                                $video["video_title"],
                                $video["video_link"],
                                new \DateTime($video["video_upload_date"]),
                                $video["id_channel"]
                                );
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $result;
    }

    public function create(object $object){
        $sql = "INSERT INTO Video   (video_title,
                                    video_link,
                                    video_upload_date,
                                    id_channel)
                                    VALUES
                                    (:title,
                                    :link,
                                    :upload_date,
                                    :id_channel)";
        try{
            $statement = $this->connexion->prepare($sql);
            MyFunctions::bindAllVideoParams($object, $statement);
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            Video::getVideoList()->offsetSet($object->getId(), $object);
            $statement->closeCursor();
        }
        catch (\PDOException $pe) {
            if ($pe->getCode()==23000) {
                $message_user = "Cette vidéo existe déjà.";
            } else {
                $message_user = "Impossible d'ajouter cette vidéo.";
            }
            echo $message_user;
            echo $pe->getMessage();
            throw new MyException($message_user, $pe->getMessage());
        }

        return $object;
    }

    public function update(object $object){
        $sql = "UPDATE Video SET    video_title = :title,
                                    video_link = :link,
                                    video_upload_date = :upload_date,
                                    id_channel = :id_channel
                            WHERE (id_video = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            MyFunctions::bindAllVideoParams($object, $statement);
            $statement->execute();
            Video::getVideoList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function delete(object $object){
        $sql = "DELETE FROM Video WHERE (id_video = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            Video::getVideoList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}