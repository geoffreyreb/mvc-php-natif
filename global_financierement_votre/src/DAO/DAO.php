<?php
namespace App\DAO;
use App\PDO\ConnectionMySQL;

abstract class DAO {

    protected $connexion;

    function __construct(){
        $this->connexion = ConnectionMySQL::getInstance();
    }

    public abstract function findAll();

    public abstract function findBy($column, $value);

    public abstract function create(Object $object);

    public abstract function update(Object $object);

    public abstract function delete(Object $object);
}
