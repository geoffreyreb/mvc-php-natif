<?php

namespace App\DAO;
use App\Model\Channel;
use App\Utilities\MyFunctions;

class ChannelDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM Channel";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $channels = $statement->fetchAll();

            foreach ($channels as $channel){
                $result = new Channel($channel["id_channel"],
                                    $channel["channel_name"]
                                    );
                Channel::getChannelList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM Channel WHERE :column = :value";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->bindParam(":column", $column);
            $statement->bindParam(":value", $value);
            $statement->execute();

            $channel = $statement->fetch();
            if (!$channel){
                // TODO //
            }

            $result = new Channel($channel["id_channel"],
                                    $channel["channel_name"]
                                    );
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $result;
    }

    public function create(object $object){
        $sql = "INSERT INTO Channel (channel_name) VALUE
                                    (:name)";
        try{
            $statement = $this->connexion->prepare($sql);
            $name = $object->getName();
            $statement->bindParam(":name", $name);
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            Channel::getChannelList()->offsetSet($object->getId(), $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }
    public function update(object $object){
        $sql = "UPDATE Channel SET  channel_name = :name
                            WHERE (id_channel = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            $statement->bindParam(":name", $object->getName());
            $statement->execute();
            Channel::getChannelList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function delete(object $object){
        $sql = "DELETE FROM Channel WHERE (id_channel = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            Channel::getChannelList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}