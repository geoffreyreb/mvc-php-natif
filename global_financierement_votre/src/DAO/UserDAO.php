<?php

namespace App\DAO;
use App\Model\User;
use App\Utilities\MyFunctions;
use App\Utilities\MyException;
use PDOException;

class UserDAO extends DAO{
    public function findAll(){
        $sql = "SELECT * FROM `User`";
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $users = $statement->fetchAll();

            foreach ($users as $user){
                $result = $this->instanceUser($user);
                User::getUserList()->offsetSet($result->getId(), $result);
            }
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    public function findBy($column, $value){
        $sql = "SELECT * FROM User WHERE " . $column . " = " . $value;
        try{
            $statement = $this->connexion->prepare($sql);

            $statement->execute();
            
            $user = $statement->fetch();
            if (!$user){
                throw new MyException(
                    "Impossible de trouver l'utilisateur",
                    "user not found with column : " . $column . " and value : " . $value);
            }

            $result = $this->instanceUser($user);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            throw new MyException(
                "Impossible d'obtenir la liste des utilisateurs",
                $t->getMessage()
            );

        }
        return $result;
    }

    public function findPermissions(object $object){
        $sql = "SELECT * FROM Permission
                INNER JOIN ACL ON Permission.id_permission = ACL.id_permission
                INNER JOIN Role_by_user ON ACL.id_role = Role_by_user.id_role
                INNER JOIN User ON Role_by_user.id_user = User.id_user
                WHERE User.id_user = " . $object->getId();
        try{
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $permissions = $statement->fetchAll();
            foreach ($permissions as $permission){
                $result[$permission[0]] = $permission[1];
            }
            $statement->closeCursor();
            return $result;
        }
        catch(\Throwable $t){
            //todo//
        }
    }

    public function create(object $object){
        $sql = "INSERT INTO `User`(username,
                    user_firstname,
                    user_lastname,
                    user_mail,
                    user_address,
                    user_password,
                    user_authentication_token,
                    user_authentication_lifespan,
                    user_reset_token,
                    user_reset_expiration) VALUES (
                        :username,
                        :first_name,
                        :last_name,
                        :email,
                        :address,
                        :password,
                        :authentication_token,
                        :authentication_expiration,
                        :reset_token,
                        :reset_expiration
                    )";
        try{
            $statement = $this->connexion->prepare($sql);
            MyFunctions::bindAllUserParams($object, $statement);
            $statement->execute();
            $id = $this->connexion->lastInsertId();
            $object->setId($id);
            $statement->closeCursor();
            $sql = "INSERT INTO Role_by_user(id_user, id_role) VALUES ( " . $id . ", 2 )";
            $statement = $this->connexion->prepare($sql);
            $statement->execute();
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function update(object $object){
        $sql = "UPDATE `User` SET   username = :username,
                                    user_firstname = :firstname,
                                    user_lastname = :lastname,
                                    user_mail = :email,
                                    user_address = :address,
                                    user_password = :password,
                                    user_authentication_token = :authentication_token,
                                    user_authentication_lifespan = :authentication_expiration,
                                    user_reset_token = :reset_token,
                                    user_reset_expiration = :reset_expiration
                                    WHERE (id_user = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id);
            MyFunctions::bindAllUserParams($object, $statement);
            $statement->execute();
            User::getUserList()->offsetSet($id, $object);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
        return $object;
    }

    public function delete(object $object){
        $sql = "DELETE FROM `User` WHERE (id_user = :id)";
        try{
            $statement = $this->connexion->prepare($sql);
            $id = $object->getId();
            $statement->bindParam(":id", $id, \PDO::PARAM_INT);
            $statement->execute();
            User::getUserList()->offsetUnset($id);
            $statement->closeCursor();
        }
        catch (\Throwable $t){
            // TODO //
        }
    }

    private function instanceUser($user){
        try{
            return new User(
                $user["id_user"],
                $user["username"],
                $user["user_password"],
                $user["user_mail"],
                $user["user_firstname"],
                $user["user_lastname"],
                $user["user_address"],
                $user["user_authentication_token"],
                $user["user_authentication_lifespan"],
                $user["user_reset_token"],
                $user["user_reset_expiration"]
            );
        }
        catch (\Throwable $t){
            // TODO //
        }
    }
}
