<?php
namespace App\Utilities;

use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MyLogger{

    private static Logger $logger;
    private static string $channel;
    private static string $LOG_PATH;
    private static LineFormatter $formatter;
    private static ?array $messageUserList;

    private const APP_NAME = '/global_financierement_votre.';

    /**
     * @param $channel
     */
    public function __construct($channel) {
        if(!isset(self::$logger)){
            self::$logger = new Logger('GFV Log');
            self::setChannel($channel);
        }
    }

    /**
     * @return Logger
     */
    public static function getInstance(): Logger
    {
        return self::$logger;
    }

    /**
     * @param string $channel
     * @return void
     */
    private static function setChannel(string $channel): void
    {
        self::$channel = strtolower($channel);
    }

    /**
     * @return string
     */
    private static function getChannel(): string
    {
        return self::$channel;
    }

    /**
     * @return string
     */
    private static function getLOGPATH(): string
    {
        self::$LOG_PATH = realpath("../../tmp/");
        return self::$LOG_PATH;
    }

    /**
     * @return LineFormatter
     */
    public static function getFormatter(): LineFormatter
    {
        $dateFormat = "d-m-Y H:i:s";
        $output = "%datetime% [%level_name%] %message% %context% %extra%\n";
        self::$formatter = new LineFormatter($output, $dateFormat);
        return self::$formatter;
    }

    /**
     * @param $debug
     * @return void
     */
    public function logDebug($debug) :void
    {
        $stream = new StreamHandler(self::getLOGPATH() . self::APP_NAME . self::getChannel() . '.log', Logger::DEBUG);
        $stream->setFormatter(self::getFormatter());
        self::$logger->pushHandler($stream);
        self::$logger->debug($debug);
    }

    /**
     * @param $info
     * @return void
     */
    public function logInfo($info) :void
    {
        $stream = new StreamHandler(self::getLOGPATH() . self::APP_NAME . self::getChannel() . '.log', Logger::INFO);
        $stream->setFormatter(self::getFormatter());
        self::$logger->pushHandler($stream);
        self::$logger->info($info);
    }

    /**
     * @param $warn
     * @return void
     */
    public function logWarn($warn) :void
    {
        $stream = new StreamHandler(self::getLOGPATH() . self::APP_NAME . self::getChannel() . '.log', Logger::WARNING);
        $stream->setFormatter(self::getFormatter());
        self::$logger->pushHandler($stream);
        self::$logger->warning($warn);
    }

    /**
     * @param $error
     * @return void
     */
    public function logError($error) :void
    {
        $stream = new StreamHandler( self::getLOGPATH() . self::APP_NAME . self::getChannel() . '.log', Logger::ERROR);
        $stream->setFormatter(self::getFormatter());
        self::$logger->pushHandler($stream);
        self::$logger->error($error);
    }

    /**
     * @return array|null
     */
    public static function getMessageUserList(): ?array
    {
        if (!isset(self::$messageUserList)) {
            self::$messageUserList = null;
        }
        return self::$messageUserList;
    }

    /**
     * @param array|null $messageUserList
     */
    public static function setMessageUserList(?array $messageUserList): void
    {
        self::$messageUserList = $messageUserList;
    }

    /**
     * @param $message
     * @param $level
     * @return void
     */
    public static function addMessageUserList($message, $level) : void
    {
        if (!isset(self::$messageUserList)) {
            self::setMessageUserList(array($level => $message));
        } else {
            self::getMessageUserList()[$level] = $message;
        }
    }
}
