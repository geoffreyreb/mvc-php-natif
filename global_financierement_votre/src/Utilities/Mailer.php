<?php

namespace App\Utilities;

use App\Utilities\MyException;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Mailer{
    private static PHPMailer $mail;
    private static string $template;
    private static string $body = '';

    private function __construct()
    {
        trigger_error('Le clonage n\'est pas autorisé.', E_USER_ERROR);
    }

    public static function getInstance (): PHPMailer
    {
        if (!isset(self::$mail)) {
            try {
                self::$mail = new PHPMailer(true);
                self::$mail->isSMTP();
                self::$mail->SMTPDebug = 0;
                self::$mail->CharSet   = 'UTF-8';
                self::$mail->SMTPAuth  = true;
                self::$mail->setFrom('support@gfv.fr', 'Global Financièrement Votre');

                if (!empty(getenv('MAIL_HOST'))
                    && !empty(getenv('MAIL_PORT'))) {
                    self::$mail->Host = getenv('MAIL_HOST');
                    self::$mail->Port = getenv('MAIL_PORT');
                   
                   

                    if (self::$mail->SMTPAuth
                        && !empty(getenv('MAIL_USERNAME'))
                        && !empty(getenv('MAIL_PASSWORD'))) {
                            
                        self::$mail->SMTPSecure = 'ssl';                      //Protocole de sécurisation des échanges avec le SMTP
                        self::$mail->Username   = getenv('MAIL_USERNAME');    //Adresse email à utiliser
                        self::$mail->Password   = getenv('MAIL_PASSWORD');    //Mot de passe de l'adresse email à utiliser
                    }
                }
            }
            catch (\Exception $e){
                throw new MyException(
                    "Une erreur est arrivée lors de l'envoi du mail.",
                    $e->getMessage()
                );
            }
        
            
        }
        return self::$mail;
    }
// gere l'envoi à des destinataires multiple ou pas
    public static function sendMailTemplated (
        string $subject,
        string|array $emailRecipient,
        string $nameRecipient = '',
        array $attachments = array(),
        array $embedImages = array(),
        $hiddenRecipient = null) :void
    {
        if (!empty($emailRecipient)) {
            if (is_array($emailRecipient)) {
                foreach ($emailRecipient as $email) {
                    self::sendMail($subject,
                        $email,
                        $nameRecipient,
                        $attachments,
                        $embedImages,
                        $hiddenRecipient);
                }
            } else {
                self::sendMail($subject,
                    $emailRecipient,
                    $nameRecipient,
                    $attachments,
                    $embedImages,
                    $hiddenRecipient);
            }
        }
    }
// envoi un et un seul mail
    public static function sendMail(
        string $subject,
        string|array $emailRecipient,
        string $nameRecipient = '',
        array $attachments = array(),
        array $embedImages = array(),
        $hiddenRecipient = null) {

        try {
            $mail = self::getInstance();
            $mail->ClearAllRecipients();
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            foreach ($embedImages as $embed) {
                $mail->AddEmbeddedImage($embed[0], $embed[1]);
            }

            foreach ($attachments as $attachment) {
                $mail->addAttachment($attachment);
            }

            if (empty($nameRecipient)) {
                $nameRecipient = $emailRecipient;
            }

            $mail->addAddress($emailRecipient, $nameRecipient);


            if (!empty($hiddenRecipient)){
                $mail->addBCC($hiddenRecipient);
            }


            $mail->Subject = $subject;
            $mail->msgHTML(self::$body);

            if (!$mail->send()) {
                throw new MyException(
                    "Impossible d'envoyer un mail à ce compte",
                    "error");
            }
        } catch (Exception $e) {
            throw new MyException(
                "Impossible d'envoyer un mail, contactez votre administrateur",
                $e->getMessage());
        }
    }

    public static function compose($args) :void
    {
        if (is_array($args)) {
            foreach ($args as $key => $value) {
                if (!is_array($value)) {
                    self::$body = preg_replace(
                        '/{{'.$key.'}}/',
                        $value,
                        self::$body);
                }
            }
        }
    }

    public static function getTemplate() :string
    {
        return self::$template;
    }

    /**
     * @param string $path
     */
    public static function setTemplate(string $path) :void
    {
        self::$template = $path;
        self::$body = file_get_contents(self::$template);
    }




}