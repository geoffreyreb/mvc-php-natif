<?php

namespace App\Utilities;
use App\Model\User;

class MyFunctions{

    public static function getRequestData($name): string
    {
        if (isset($_GET[$name])) {
            $_GET[$name] = htmlentities(
                $_GET[$name],
                ENT_QUOTES,
                'UTF-8');
            return $_GET[$name];

        } else if (isset($_POST[$name])) {
            $_POST[$name] = htmlentities(
                $_POST[$name],
                ENT_QUOTES,
                'UTF-8');
            return $_POST[$name];

        } else {
            // TODO //
        }
    }


    public static function bindAllUserParams($object, $statement){
        try{
            $username = $object->getUsername();
            $password = $object->getPassword();
            $email = $object->getEmail();
            $first_name = $object->getFirst_name();
            $last_name = $object->getLast_name();
            $address = $object->getAddress();
            $auth = $object->getAuthentication_token();
            $auth_expire = $object->getAuthentication_expiration();
            $reset = $object->getReset_token();
            $reset_expire = $object->getReset_expiration();
            $statement->bindParam(':username', $username);
            $statement->bindParam(':password', $password);
            $statement->bindParam(':email', $email);
            $statement->bindParam(':first_name', $first_name);
            $statement->bindParam(':last_name', $last_name);
            $statement->bindParam(':address', $address);
            $statement->bindParam(':authentication_token', $auth);
            $statement->bindParam(':authentication_expiration', $auth_expire);
            $statement->bindParam(':reset_token', $reset);
            $statement->bindParam(':reset_expiration', $reset_expire);
        }
        catch (\Throwable $t){
            throw new MyException("Une erreur est survenue", $t->getMessage());
        }
    }

    public static function bindAllVideoParams($object, $statement){
        $title = $object->getTitle();
        $link = $object->getLink();
        $date = $object->getUpload_date()->format('Y-m-d h:m:s');
        $channel = $object->getId_channel();
        
        $statement->bindParam(':title', $title);
        $statement->bindParam(':link', $link);
        $statement->bindParam(':upload_date', $date);
        $statement->bindParam(':id_channel', $channel);
    }

    public static function bindAllArticleParams($object, $statement){
        $statement->bindParam(':title', $object->getTitle());
        $statement->bindParam(':content', $object->getContent());
        $statement->bindParam(':summary', $object->getSummary());
        $statement->bindParam(':image_path', $object->getImage_path());
        $statement->bindParam(':image_description', $object->getImage_description());
        $statement->bindParam(':date', $object->getDate());
        $statement->bindParam(':id_user', $object->getId_user());
    }
}
