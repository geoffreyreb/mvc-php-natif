<?php

namespace App\Utilities;
use App\DAO\UserDAO;
use App\Utilities\MyException;

class Security{

    // Stores the lifespan of the authentication token : 2 weeks in seconds
    public const AUTHENTICATION_TOKEN_LIFESPAN = 2 * 7 * 24 * 60 * 60;
    private const AUTHENTICATION_TOKEN = "authentication-token";
    private const AUTHENTICATION_TOKEN_LENGTH = 24;

    // Stores the lifespan of the password reset token : 30 minutes in seconds
    public const RESET_TOKEN_LIFESPAN = 30 * 60;
    private const RESET_TOKEN_LENGTH = 24;

    private const CSRF_TOKEN_LENGTH = 90;
    public const CSRF_TOKEN = "csrf-token";

    private function __construct(){}

    private static function generateToken(int $length){
        return password_hash(random_bytes($length), PASSWORD_BCRYPT);
    }

    public static function generateAuthenticationTokenCookie(){
        $token = self::generateToken(self::AUTHENTICATION_TOKEN_LENGTH);
        setcookie(self::AUTHENTICATION_TOKEN, $token, time()+self::AUTHENTICATION_TOKEN_LIFESPAN);
        return $token;
    }

    public static function deleteAuthenticationTokenCookie()
    {
        setcookie(self::AUTHENTICATION_TOKEN);
    }

    public static function getAuthenticationTokenCookie(): mixed
    {
        if (isset($_COOKIE[self::AUTHENTICATION_TOKEN])) {
            return $_COOKIE[self::AUTHENTICATION_TOKEN];
        }
        else{
            return null;
        }
    }

    public static function generateCSRFToken(){
        $token = self::generateToken((self::CSRF_TOKEN_LENGTH));
        $_SESSION[Security::CSRF_TOKEN] = $token;
        return $token;
    }

    public static function generateResetToken () :string
    {
        return self::generateToken(self::RESET_TOKEN_LENGTH);
    }


    public static function verifyAuthenticationCookie(){
        $cookie = Security::getAuthenticationTokenCookie();

        if ($cookie != null){
            try{
                $dao = new UserDAO();
                $user = $dao->findBy("user_authentication_token", $cookie);
                $date=$user->getAuthentication_expiration()->format('Y-m-d');
                $now = new \DateTime('now');
                $now= $now->format('Y-m-d');

                if ($cookie != null && $user->getAuthentication_token() != null
                    && $date > $now){
                        $_SESSION['user'] = $user;
                        $now = new \DateTime('now');
                        $now->modify('+'.Security::AUTHENTICATION_TOKEN_LIFESPAN."second");
                        $user->setAuthentication_expiration($now);
                        $dao->update($user);
                }   
                else{
                    ?>
                    <script>window.alert("Votre session a expiré, veuillez vous reconnecter")</script>
                    <?php
                }
            } catch(\PDOException $pe){
                throw new MyException(
                    "Impossible de récupérer le cookie d'authenitification",
                    $pe->getMessage()
                );
            }
        }
    }
}
