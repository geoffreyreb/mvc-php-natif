<?php

namespace App\Utilities;

class AccessException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Trying to access to denied page");
    }

}
