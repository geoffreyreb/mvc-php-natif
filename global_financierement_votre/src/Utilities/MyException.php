<?php

namespace App\Utilities;

class MyException extends \Exception{

    private string $user_message;
    
    public function __construct(string $user_message, ?string $dev_message)
    {
        parent::__construct($dev_message);
        $this->setUser_message($user_message);
    }

    /**
     * Get the value of user_message
     */ 
    public function getUser_message()
    {
        return $this->user_message;
    }

    /**
     * Set the value of user_message
     *
     * @return  self
     */ 
    public function setUser_message($user_message)
    {
        $this->user_message = $user_message;

        return $this;
    }
}
