<?php
namespace App\Model;

use ArrayObject;
use DateTime;

class Article{
    private \ArrayObject $articleList;

    private int $id;
    private string $title;
    private string $content;
    private string $summary;
    private string $image_path;
    private string $image_description;
    private \DateTime $date;
    private int $id_user;
    public function __construct(?int $p_id, string $p_title, string $p_content, string $p_summary,
                                string $p_image_path, string $p_image_description,
                                \DateTime $p_date, int $p_id_user)
    {
        $this->setId($p_id);
        $this->setTitle($p_title);
        $this->setContent($p_content);
        $this->setSummary($p_summary);
        $this->setImage_path($p_image_path);
        $this->setImage_description($p_image_description);
        $this->setDate($p_date);
        $this->setId_user($p_id_user);

        self::getArticleList()->offsetSet($this->getId(), $this);
    }



    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of title
     * @return string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */ 
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get the value of content
     * @return string
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     */ 
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get the value of summary
     * @return string
     */ 
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set the value of summary
     */ 
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }

    /**
     * Get the value of image_path
     * @return string
     */ 
    public function getImage_path()
    {
        return $this->image_path;
    }

    /**
     * Set the value of image_path
     */ 
    public function setImage_path($image_path)
    {
        $this->image_path = $image_path;
    }

    /**
     * Get the value of image_description
     * @return string
     */ 
    public function getImage_description()
    {
        return $this->image_description;
    }

    /**
     * Set the value of image_description
     */ 
    public function setImage_description($image_description)
    {
        $this->image_description = $image_description;
    }

    /**
     * Get the value of date
     * @return \Datetime
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     */ 
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get the value of id_user
     * @return int
     */ 
    public function getId_user()
    {
        return $this->id_user;
    }

    /**
     * Set the value of id_user
     */ 
    public function setId_user($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * Get the value of articleList
     * @return \ArrayObject
     */ 
    public static function getArticleList()
    {
        if(!isset(self::$articleList)){
            self::setArticleList(new \ArrayObject());
        }
        return self::$articleList;
    }

    /**
     * Set the value of articleList
     */ 
    public function setArticleList($articleList)
    {
        $this->articleList = $articleList;
    }

}
