<?php
namespace App\Model;

use ArrayObject;

class Role
{
    private ?int $id;
    private string $role_label;
    private static \ArrayObject $roleList;


    public function __construct(?int $p_id, string $p_label)
    {
        $this->setId($p_id);
        $this->setRole_Label($p_label);
    }

   

    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of Role_label
     * @return string
     */ 
    public function getRole_label()
    {
        return $this->role_label;
    }

    /**
     * Set the value of Role_label
     */ 
    public function setRole_label($role_label)
    {
        $this->role_label = $role_label;
    }

    /**
     * Get the value of roleList
     * @return \ArrayObject
     */ 
    public static function getRoleList()
    {
        if(!isset(self::$roleList)){
            self::setRoleList(new \ArrayObject());
        }
        return self::$roleList;
    }

    /**
     * Set the value of roleList
     */ 
    public static function setRoleList($roleList)
    {
        self::$roleList = $roleList;
    }
}