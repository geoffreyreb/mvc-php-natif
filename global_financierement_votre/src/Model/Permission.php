<?php
namespace App\Model;

class Permission{
    private static \ArrayObject $permissionList;

    private int $id;
    private string $label;

    public function __construct(?int $p_id, string $p_label)
    {
        $this->setId($p_id);
        $this->setLabel($p_label);

        self::getPermissionList()->offsetSet($this->getId(), $this);
    }

    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of label
     * @return string
     */ 
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of label
     */ 
    public function setLabel($label)
    {
        $this->label = $label;
    }
       
    /**
     * Get the value of permissionList
     * @return \ArrayObject
     */ 
    public static function getPermissionList()
    {
        if(!isset(self::$permissionList)){
            self::setPermissionList(new \ArrayObject());
        }
        return self::$permissionList;
    }

    /**
     * Set the value of permissionList
     */ 
    public static function setPermissionList($permissionList)
    {
        self::$permissionList = $permissionList;
    }

}