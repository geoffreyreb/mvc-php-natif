<?php
namespace App\Model;

class User{
    private static \ArrayObject $userList;

    private ?int $id;
    private string $username;
    private string $password;
    private string $email;
    private string $first_name;
    private string $last_name;
    private string $address;
    private ?string $authentication_token;
    private ?\datetime $authentication_expiration;
    private ?string $reset_token;
    private ?\datetime $reset_expiration;

    public function __construct(?int $p_id, string $p_username, string $p_password, string $p_email,
                                string $p_first_name, string $p_last_name, string $p_address,
                                ?string $p_authentication_token=null, ?\datetime $p_authentication_expiration=null,
                                ?string $p_reset_token=null, ?\datetime $p_reset_expiration=null)
    {
        $this->setId($p_id);

        $this->setUsername($p_username);
        $this->setPassword($p_password);
        $this->setEmail($p_email);

        $this->setFirst_name($p_first_name);
        $this->setLast_name($p_last_name);
        $this->setAddress($p_address);

        $this->setAuthentication_token($p_authentication_token);
        $this->setAuthentication_expiration($p_authentication_expiration);
        $this->setReset_token($p_reset_token);
        $this->setReset_expiration($p_reset_expiration);

        self::getUserList()->offsetSet($this->getId(), $this);
    }

    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of username
     * @return string
     */ 
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the value of username
     */ 
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get the value of password
     * @return string
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     */ 
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get the value of email
     * @return string
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     */ 
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get the value of first_name
     * @return string
     */ 
    public function getFirst_name()
    {
        return $this->first_name;
    }

    /**
     * Set the value of first_name
     */ 
    public function setFirst_name($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * Get the value of last_name
     * @return string
     */ 
    public function getLast_name()
    {
        return $this->last_name;
    }

    /**
     * Set the value of last_name
     */ 
    public function setLast_name($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * Get the value of address
     * @return string
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     */ 
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get the value of authentication_token
     * @return string
     */ 
    public function getAuthentication_token()
    {
        return $this->authentication_token;
    }

    /**
     * Set the value of authentication_token
     */ 
    public function setAuthentication_token($authentication_token)
    {
        $this->authentication_token = $authentication_token;
    }

    /**
     * Get the value of authentication_expiration
     * @return \datetime
     */ 
    public function getAuthentication_expiration()
    {
        return $this->authentication_expiration;
    }

    /**
     * Set the value of authentication_expiration
     */ 
    public function setAuthentication_expiration($authentication_expiration)
    {
        $this->authentication_expiration = $authentication_expiration;
    }

    /**
     * Get the value of reset_token
     * @return string
     */ 
    public function getReset_token()
    {
        return $this->reset_token;
    }

    /**
     * Set the value of reset_token
     */ 
    public function setReset_token($reset_token)
    {
        $this->reset_token = $reset_token;
    }

    /**
     * Get the value of reset_expiration
     * @return \datetime
     */ 
    public function getReset_expiration()
    {
        return $this->reset_expiration;
    }

    /**
     * Set the value of reset_expiration
     */ 
    public function setReset_expiration($reset_expiration)
    {
        $this->reset_expiration = $reset_expiration;
    }

    /**
     * Get the value of userList
     * @return \ArrayObject
     */ 
    public static function getUserList()
    {
        if(!isset(self::$userList)){
            self::setUsersList(new \ArrayObject());
        }
        return self::$userList;
    }

    /**
     * Set the value of userList
     */
    public static function setUsersList(\ArrayObject $users)
    {
        self::$userList = $users;
    }

}