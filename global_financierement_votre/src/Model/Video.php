<?php
namespace App\Model;

class Video{
    private static \ArrayObject $videoList;

    private int $id;
    private string $title;
    private string $link;
    private \DateTime $upload_date;
    private int $id_channel;
    public function __construct(?int $p_id, string $p_title, string $p_link,
                                \DateTime $p_upload_date, int $p_id_channel)
    {
        $this->setId($p_id);
        $this->setTitle($p_title);
        $this->setLink($p_link);
        $this->setUpload_date($p_upload_date);
        $this->setId_channel($p_id_channel);

        self::getVideoList()->offsetSet($this->getId(), $this);
    }

    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of title
     * @return string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */ 
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get the value of link
     * @return string
     */ 
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set the value of link
     */ 
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Get the value of upload_date
     * @return \DateTime
     */ 
    public function getUpload_date()
    {
        return $this->upload_date;
    }

    /**
     * Set the value of upload_date
     */ 
    public function setUpload_date($upload_date)
    {
        $this->upload_date = $upload_date;
    }
    /**
     * Get the value of id_channel
     * @return int
     */ 
    public function getId_channel()
    {
        return $this->id_channel;
    }

    /**
     * Set the value of id_channel
     */ 
    public function setId_channel($id_channel)
    {
        $this->id_channel = $id_channel;
    }
    
    /**
     * Get the value of videoList
     * @return \ArrayObject
     */ 
    public static function getVideoList()
    {
        if(!isset(self::$videoList)){
            self::setVideoList(new \ArrayObject());
        }
        return self::$videoList;
    }

    /**
     * Set the value of videoList
     */ 
    public static function setVideoList($videoList)
    {
        self::$videoList = $videoList;
    }

}