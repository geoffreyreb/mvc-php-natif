<?php
namespace App\Model;

class Package{
    private static \ArrayObject $packageList;

    private int $id;
    private string $name;
    public function __construct(?int $p_id, string $p_name)
    {
        $this->setId($p_id);
        $this->setName($p_name);

        self::getPackageList()->offsetSet($this->getId(), $this);
    }

    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of name
     * @return string
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     */ 
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the value of packageList
     * @return \ArrayObject
     */ 
    public static function getPackageList()
    {
        if(!isset(self::$packageList)){
            self::setPackageList(new \ArrayObject());
        }
        return self::$packageList;
    }

    /**
     * Set the value of packageList
     */ 
    public function setPackageList($packageList)
    {
        $this->packageList = $packageList;
    }

}