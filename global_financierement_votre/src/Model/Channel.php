<?php
namespace App\Model;

class Channel{
    private static \ArrayObject $channelList;

    private int $id;
    private string $name;

    public function __construct(?int $p_id, string $p_name)
    {
        $this->setId($p_id);
        $this->setName($p_name);

        self::getChannelList()->offsetSet($this->getId(), $this);
    }

    /**
     * Get the value of id
     * @return int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of name
     * @return string
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     */ 
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the value of channelList
     * @return \ArrayObject
     */ 
    public static function getChannelList()
    {
        if(!isset(self::$channelList)){
            self::setChannelList(new \ArrayObject());
        }
        return self::$channelList;
    }

    /**
     * Set the value of channelList
     */ 
    public static function setChannelList($channelList)
    {
        self::$channelList = $channelList;
    }
}