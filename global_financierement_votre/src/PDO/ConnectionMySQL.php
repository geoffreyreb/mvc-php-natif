<?php
namespace App\PDO;

class ConnectionMySQL{
    private static string $url;
    private static string $user;
    private static string $password;
    private static \PDO $mySQLConnection;

    private function __construct(){
        trigger_error("Le clonage n'est pas autorisé.", E_USER_ERROR);
    }

    public static function getInstance()
    {
        if (!isset(self::$mySQLConnection)) {
            try {
                self::$mySQLConnection = new \PDO(self::getUrl(), self::getUser(), self::getPassword());
            } catch (\Throwable $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }
        return self::$mySQLConnection;
    }

     /**
     * @return string
     */
    public static function getUrl(): string
    {
        self::$url = 'mysql:host='.getenv('HOST_NAME')
            .';dbname='.getenv('MY_DATABASE')
            .';charset=utf8';
        return self::$url;
    }

    /**
     * @return string
     */
    public static function getUser(): string
    {
        self::$user = getenv('MYSQL_USER');
        return self::$user;
    }

    /**
     * @return string
     */
    public static function getPassword(): string
    {
        self::$password = getenv('MYSQL_PASSWORD');
        return self::$password;
    }

    /**
     * @return \PDO
     */
    public static function getMySQLConnection(): \PDO
    {
        return self::$mySQLConnection;
    }



}