<?php

use App\Utilities\Mailer;
use App\DAO\UserDAO;
use App\Utilities\MyException;
use App\Utilities\Security;

function index(){
    $dao = new UserDAO();
    try{
        if (!empty($_POST['mail'])){
            $mail = htmlentities($_POST['mail'],ENT_QUOTES,'UTF-8');
            $daomail = '"' . $mail . '"';
            if ($_SESSION[Security::CSRF_TOKEN] == $_POST[Security::CSRF_TOKEN]){

                $user = $dao->findBy("user_mail", $daomail);
                $token = Security::generateResetToken();
                $user->setReset_token($token);
                $now = new \DateTime;
                $user->setReset_expiration($now->modify("+" . Security::RESET_TOKEN_LIFESPAN . "second"));
                $dao->update($user);

                Mailer::setTemplate(realpath("../src/view/template/mail.php"));
                Mailer::compose(array(
                    "name" => $user->getFirst_name(),
                    "token" => urlencode($token)
                ));
                Mailer::sendMailTemplated("Changement de mot de passe", $mail);
            }


        } elseif (!empty($_GET['token'])) {
            $user_token = '"' . $_GET['token'] . '"';
            $user = $dao->findBy("user_reset_token", $user_token);

            $date=$user->getReset_expiration()->getTimestamp();
            $now = new \DateTime();
            $now= $now->getTimestamp();
            //If user have reset date witch goes after now
            if ( $date > $now )
                {
                    $_SESSION['user-forgotaccount'] = $user;

            } else {
                //token is outdated, let's remove it from the database
                $user->setReset_token(null);
                $user->setReset_expiration(null);
                $dao->update($user);

                throw new MyException(
                    "Votre mail a expiré ou compte introuvable.",
                    "error");
            }
            
        }

        elseif (!empty($_POST['password'])) {
            $password= password_hash(htmlentities($_POST['password'],ENT_QUOTES,'UTF-8').getenv('PEPPER'), PASSWORD_BCRYPT);

            $user = $_SESSION['user-forgotaccount'];
            $user->setPassword($password);
            $user->setReset_token(null);
            $user->setReset_expiration(null);
            $dao->update($user);
            $_SESSION['user-forgotaccount'] = null;
            header("location:../login");
            exit();
        }


        
    }
    catch (MyException $e){
        throw new MyException(
            $e->getUser_message(),
            $e->getMessage()
        );
    }
    require(realpath('../src/view/passwordForgotten.php'));
}
