<?php
use App\Utilities\MyException;
use App\DAO\UserDAO;
use App\DAO\VideoDAO;
use App\DAO\RoleDAO;
use App\DAO\PermissionDAO;
use App\DAO\ChannelDAO;
use App\Model\User;
use App\Model\Video;
use App\Model\Role;
use App\Model\Permission;
use App\Model\Channel;
use App\Utilities\MyFunctions;
use App\Utilities\MyLogger;

function index(){
    $userDAO = new UserDAO;
    $videoDAO = new VideoDAO;
    $roleDAO = new RoleDAO;
    $permissionDAO = new PermissionDAO;
    $channelDAO = new ChannelDAO;

    try{
        if (isset($_POST['video'])){
            $video = new Video(0, $_POST['title'], $_POST['link'], new \DateTime($_POST['date']), $_POST['channel']);
            $videoDAO->create($video);
            MyLogger::addMessageUserList("Votre vidéo a bien été ajoutée au site", "upload successful");
        }
        else if (isset($_POST['user-update'])){
            $user = $userDAO->findBy('id_user', $_POST['user-update']);
            $role = $roleDAO->findBy('id_role', $_POST['role-update']);
            $roleDAO->update_by_user($user, $role);
            MyLogger::addMessageUserList("L'utilisateur a bien été mis à jour", "update successful");
        }
        else if (isset($_POST['role-label'])){
            $role = new Role(0, $_POST['role-label']);
            $roleDAO->create($role);
            $permissionDAO->findAll();
            $all_permissions = Permission::getPermissionList();
            foreach ($all_permissions as $permission){
                if (isset($_POST[$permission->getLabel()])){
                    $permissionDAO->create_by_role($permission, $role);
                }
            }
        }
        else if (isset($_POST['new-channel'])){
            $channel = new Channel(0, $_POST['new-channel']);
            $channelDAO->create($channel);
        }
    }
    catch (MyException $me){
        $logger = new MyLogger("database");
        $logger->logError($me->getMessage());
        MyLogger::addMessageUserList($me->getUser_message(), "error");
    }

    if (isset($_SESSION['user'])){
        $permissions = $userDAO->findPermissions($_SESSION['user']);
        if (isset($permissions[1]) && $permissions[1] == "MANAGE_SITE"){
            require_once(realpath('../src/view/administration.php'));
        }
        else {
            header("location:../error?error=401");
        }
    } else {
        header("location:../error?error=401");
    }

}