<?php

use App\DAO\UserDAO;
use App\Utilities\MyException;
use App\Model\User;
use App\Utilities\Security;

function index(){
    try{
        if(!empty($_POST)){
            $username=htmlentities($_POST['username'],ENT_QUOTES,'UTF-8');
            $mail=htmlentities($_POST['mail'],ENT_QUOTES,'UTF-8');
            $password = htmlentities($_POST['pswrd'],ENT_QUOTES,'UTF-8');

            if ($_SESSION[Security::CSRF_TOKEN] == $_POST[Security::CSRF_TOKEN]){
                if (getenv('PEPPER') !== null){
                    $password= password_hash($password.getenv('PEPPER'), PASSWORD_BCRYPT);
                }
                else{
                    throw new MyException(
                        "Impossible de créer un compte, contactez l'administrateur.",
                        "Can't find pepper"
                    );
                }
                $firstname=htmlentities($_POST['firstname'],ENT_QUOTES,'UTF-8');
                $lastname=htmlentities($_POST['lastname'],ENT_QUOTES,'UTF-8');
                $address=htmlentities($_POST['address'],ENT_QUOTES,'UTF-8');

                $daoUtilisateur=new UserDAO();
                $user=new User(null, $username, $password, $mail, $firstname, $lastname, $address);
                $daoUtilisateur->create($user);
                echo 'Votre compte a bien été créé';
            }
            else{
                throw new MyException(
                    "Impossible de s'inscrire, n'essayez pas de hacker mon site !",
                    "CSRF Token not valid"
                );
            }
        }
    }catch(MyException $me){
        echo $me->getUser_message();
    }
    require(realpath('../src/view/register.php'));
}

