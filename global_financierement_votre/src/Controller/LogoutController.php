<?php

use App\Utilities\Security;

function index()
{
    Security::deleteAuthenticationTokenCookie();
    unset($_SESSION['user']);
    header("location:../");
    exit();
}
