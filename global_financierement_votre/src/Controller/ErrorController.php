<?php
use App\Utilities\MyFunctions;
function index(){
    if (isset($_GET['error'])) {
        try {
            $error = MyFunctions::getRequestData('error');
            require(realpath('../src/view/common/error/' . $error . '.php'));

        } catch (Throwable $t) {
            require(realpath('../src/view/common/error/404.php'));
        }
    } else {
        require(realpath('../src/view/common/error/404.php'));
    }
}
