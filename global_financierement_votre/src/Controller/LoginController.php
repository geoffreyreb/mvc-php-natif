<?php

use App\DAO\UserDAO;
use App\Utilities\MyException;
use App\Model\User;
use App\Utilities\Security;

function index(){
    try{
        if(!empty($_POST)){
            $username="'" . htmlentities($_POST['username'],ENT_QUOTES,'UTF-8') . "'";
            $password=htmlentities($_POST['pswrd'],ENT_QUOTES,'UTF-8');

            if ($_SESSION[Security::CSRF_TOKEN] == $_POST[Security::CSRF_TOKEN]){
            
                $daoUtilisateur=new UserDAO();
                $user = $daoUtilisateur->findBy("username", $username);
                if ($user){
                    try{
                        if (isset($_POST['stay-logged-in']) && $_POST['stay-logged-in'] === 'on'){
                            $token = Security::generateAuthenticationTokenCookie();
                            $user->setAuthentication_token($token);
                            $now = new \DateTime();
                            $user->setAuthentication_expiration($now->modify('+'.Security::AUTHENTICATION_TOKEN_LIFESPAN."second"));
                            $user->setReset_token(null);
                            $user->setReset_expiration(null);
                            $daoUtilisateur->update($user);
                        }
                    } catch (MyException $me){
                        throw new MyException($me->getUser_message(), $me->getMessage());
                    }

                    if (getenv('PEPPER') !== null){
                        
                        if ( password_verify( ($password . getenv('PEPPER')), $user->getPassword() ) ){
                            $_SESSION['user'] = $user;
                        }
                        else{
                            throw new MyException('Le mot de passe ne correspond pas à celui dans notre base de données',
                                    "wrong password");
                        }
                        header("Location: ../");
                        exit();
                    }
                    else{
                        throw new MyException(
                            "Impossible de se connecter, contactez l'administrateur.",
                            "Can't find pepper"
                        );
                    }
                }
            }
            else{
                throw new MyException(
                    "Impossible de se connecter, n'essayez pas de hacker mon site !",
                    "CSRF Token not valid"
                );
            }
        }
    }catch(MyException $me){
        echo $me->getUser_message();
    }

    require(realpath('../src/view/login.php'));
}

