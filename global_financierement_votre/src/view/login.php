<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
use App\Utilities\Security;
?>
    <form action="login" method="post">
        <fieldset>
            <legend>Se connecter</legend>
            <div>
                <label for="username">Nom d'utilisateur</label>
                <input type="text" name="username" id="username" required>
            </div>
            <div>
                <label for="pswrd">Mot de passe</label>
                <input type="password" name="pswrd" id="pswrd" required>
            </div>
            <div class="checkbox-div">
                <label for="stay-logged-in">Rester connecté ?</label>
                <input type="checkbox" name="stay-logged-in" id="stay-logged-in" class="checkbox">
            </div>
            <input type="submit" value="Se connecter" class='submit'>
            <input type="hidden" name="<?= Security::CSRF_TOKEN ?>" value="<?= Security::generateCSRFToken() ?>">
            <a href="passwordForgotten" title="Mot de passe oublié">Mot de passe oublié ? Cliquez ici.</a>
        </fieldset>
    </form>
    <p>
        <a href="register" title="S'inscrire'" id="register">Vous n'avez pas encore de compte ? Inscrivez vous.</a>
    </p>
    <?php
require_once(realpath("../src/view/common/footer.php"));
?>