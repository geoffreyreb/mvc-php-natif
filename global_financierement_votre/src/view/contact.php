<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
?>

<main>
  <h2 class="titre2">Contact</h2>

  <div class="container">
    <form action="contact" class="form" method="POST">
      <div class="row">
        <div class="col-25">
          <label for="name">Nom</label>
        </div>
        <div class="col-75">
          <input type="text" id="name" name="name" placeholder="Votre nom.." required>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="email">Email</label>
        </div>
        <div class="col-75">
          <input type="email" id="email" name="email" placeholder="Votre Email.." required>
        </div>
      </div>
      <div class="row">
      <div class="row">
        <div class="col-25">
          <label for="subject">Message</label>
        </div>
        <div class="col-75">
          <textarea id="subject" name="subject" placeholder="Votre message.." style="height:200px" required></textarea>
        </div>
      </div>
      <br>
      <div class="row">
        <input type="submit" value="Submit">

      </div>
    </form>
  </div>
</main>
<?php
require_once(realpath("../src/view/common/footer.php"));
?>