<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
?>
<div class="article1">
    <h3>Lorem Ipsum</h3>
    <p>
        Voluptate aliquip aute eu sint nostrud magna quis enim duis aliquip voluptate. Consequat cupidatat adipisicing et cillum ullamco cupidatat exercitation nisi magna aliquip elit. Sint eiusmod laborum elit Lorem eu ad voluptate velit. Consectetur cupidatat laboris reprehenderit laboris consequat. Ut sint aliqua reprehenderit ipsum.
    </p>
</div>
<div class="article2">
    <h3>Ipsum Lorem</h3>
    <p>
        Eu cillum aliquip aute Lorem sit magna minim in consectetur voluptate. Labore mollit officia qui excepteur mollit ex culpa ea nisi cupidatat deserunt ut. Officia sint amet sint non eu est cillum adipisicing nisi veniam excepteur eu nulla elit. Proident eiusmod excepteur do qui mollit.
    </p>
</div>
<div class="article1">
    <h2>Ipsem Lorum</h2>
    <p>
        Et non ex proident qui. Eu ea cillum ex tempor esse elit duis nulla ipsum officia tempor eiusmod. Incididunt id dolor Lorem ea incididunt consectetur do esse elit dolor fugiat sit proident. Voluptate veniam labore dolor non id mollit esse magna nostrud. Veniam enim esse esse sit reprehenderit excepteur dolore id. Sunt elit anim dolor anim ad consectetur anim tempor amet cupidatat. Incididunt proident reprehenderit commodo minim eiusmod magna occaecat incididunt ullamco.
    </p>
</div>
<div class="article2">
    <h2>Lorum Ipsem</h2>
    <p>
        Eiusmod est qui ad magna id occaecat esse mollit ullamco cillum occaecat irure laboris minim. Mollit duis ipsum ut commodo amet veniam anim tempor irure eu. Mollit do Lorem sit amet ullamco do. Ullamco ipsum dolore anim id. Nisi id proident dolore consequat amet aute aliquip excepteur. Reprehenderit occaecat mollit veniam non qui adipisicing. Voluptate tempor veniam consequat exercitation exercitation incididunt.
    </p>
</div>

<?php
require_once(realpath("../src/view/common/footer.php"));