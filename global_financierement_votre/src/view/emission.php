<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));

use App\DAO\VideoDAO;
use App\DAO\ChannelDAO;
use App\Model\Video;
use App\Model\Channel;

$videoDAO= new VideoDAO;
$channelDAO = new ChannelDAO;
$videoDAO->findAll();
$channelDAO->findAll();
$videos = Video::getVideoList();
$channels = Channel::getChannelList();
$count = 1;

foreach ($channels as $channel){
    if ($count == 1){
        $count = 0;
    }
    else{
        $count =1;
    }
    ?>
    <h2>
        <?= $channel->getName();?>
    </h2>
    <div class="channel-<?=$count?>">
    <?php
    foreach ($videos as $video){
        if ($video->getId_channel() == $channel->getId()){
            echo '<div class = "video"><h3>' . $video->getTitle() .'</h3>';
            echo $video->getLink() . '</div>';
        }
    }
    echo '</div>';
}
require_once(realpath("../src/view/common/footer.php"));
?>