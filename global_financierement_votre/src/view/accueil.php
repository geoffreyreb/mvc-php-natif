<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
?>
<div class="div1">
    <h2>
        Lorem Ipsum
    </h2>
    <p>
        Tempor in fugiat cillum labore ipsum irure ut irure officia pariatur. In aute ea laborum voluptate commodo adipisicing. Magna duis laboris sunt deserunt deserunt do id. Aute aliquip sit nulla cupidatat est Lorem cupidatat anim mollit Lorem aliquip non. In cupidatat excepteur reprehenderit ea aliquip irure cillum et dolore. Nulla aliqua tempor eiusmod do culpa reprehenderit. Velit cupidatat anim adipisicing occaecat ex ea commodo eiusmod.
    </p>
    <p>
        Reprehenderit consequat nostrud officia consequat labore veniam fugiat non ea occaecat magna enim et magna. Reprehenderit consectetur qui id aliquip elit laboris reprehenderit et voluptate officia cupidatat deserunt. Fugiat reprehenderit elit cupidatat pariatur voluptate voluptate veniam tempor cupidatat enim eu amet ea.
    </p>
    <p>
        Culpa adipisicing nostrud pariatur minim velit enim exercitation laboris aliquip dolor minim dolor tempor. Laboris id elit consectetur tempor aliqua nisi voluptate do irure proident esse non do. Quis fugiat eu et veniam consectetur nisi ullamco culpa aliquip magna pariatur nisi. Proident ex ad est sit irure incididunt aliquip ut. Velit nulla ipsum dolor eiusmod qui velit consectetur deserunt velit exercitation ad mollit. Ea adipisicing veniam reprehenderit aliquip quis.
    </p>
</div>
<div class="div2">
    <h2>
        Ipsum Lorem
    </h2>
    <p>
        Irure voluptate consectetur deserunt labore laborum mollit eu consequat eu labore non. Velit eiusmod nostrud eiusmod esse. Id irure tempor sunt ut proident adipisicing.
    </p>
    <p>
        Officia proident eu officia non do Lorem. Qui nulla tempor ullamco tempor minim nulla elit qui cillum exercitation quis do incididunt. Pariatur velit Lorem eu sit. Nisi elit veniam culpa non excepteur tempor sit aliqua deserunt aute anim. Nulla nisi dolor proident exercitation adipisicing adipisicing ad nostrud nulla. Pariatur do eu anim cupidatat sit consectetur mollit.
    </p>
</div>
<div class="div1">
    <h2>
        Ipsem Lorum
    </h2>
    <p>
        Voluptate velit laborum ex culpa officia nisi magna aliqua occaecat consequat mollit duis excepteur consequat. Occaecat elit aute proident magna velit magna sit et. Dolor occaecat in ut amet. Mollit qui consequat velit et deserunt. Tempor non nisi fugiat Lorem deserunt irure cupidatat. Deserunt exercitation Lorem anim elit commodo. Labore laborum minim elit commodo et ex non eiusmod laboris veniam.
    </p>
    <p>
        Ex sint ut laboris veniam labore nisi proident dolore est sint. Nostrud culpa laborum ad consequat et ea amet sint veniam consectetur sunt qui nostrud elit. Sit aliqua consequat ipsum tempor elit adipisicing enim eu tempor nostrud. Sint cupidatat labore id aliquip eu fugiat eiusmod in anim in id. Non est quis veniam id laboris anim ut sunt. Aliqua nostrud anim commodo consequat tempor minim aliquip culpa.
    </p>
    <p>
        Aliquip mollit excepteur qui irure qui ullamco anim laborum. Qui excepteur consequat fugiat velit irure dolore ad occaecat. Lorem nisi magna ullamco pariatur laborum Lorem labore nulla.
    </p>
    <p>
        Ex reprehenderit dolore quis est incididunt officia anim anim sit in et ipsum sint. Consectetur sit exercitation fugiat duis quis consectetur commodo magna eu ipsum consectetur non dolor. Ipsum cillum nisi duis est non esse ad consequat id dolore. Ea occaecat magna occaecat minim nostrud occaecat ad esse excepteur occaecat anim.
    </p>
</div>

<?php
require_once(realpath("../src/view/common/footer.php"));