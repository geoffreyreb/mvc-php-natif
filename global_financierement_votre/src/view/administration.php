<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
use App\DAO\ChannelDAO;
use App\DAO\UserDAO;
use App\DAO\RoleDAO;
use App\DAO\PermissionDAO;
use App\Model\Channel;
use App\Model\User;
use App\Model\Role;
use App\Model\Permission;
?>
<h2>Insérer une vidéo youtube</h2>
<form action="administration" method="POST" class="form1">
    <fieldset>
        <input type="hidden" name="video">
        <div class="form-div">
            <label for="title">Titre de la vidéo</label>
            <input type="text" name="title" id="title" required>
        </div>
        <div class="form-div">
            <label for="date">Date de mise en ligne de la vidéo</label>
            <input type="date" name="date" id="date" required>
        </div>
        <div class="form-div">
        <label for="channel">Chaîne youtube</label>
        <select name="channel" id="channel" required>
            <option value="">-- Choisir une chaîne --></option>
            <?php
                $channelDAO = new ChannelDAO;
                $channelDAO->findAll();
                $channels = Channel::getChannelList();
                foreach ($channels as $channel){
                    echo '<option value="' . $channel->getId() .'">' . $channel->getName() . '</option>';
                }
            ?>
        </select>
        </div>
        <div class="form-div">
            <label for="link">Code d'intégration</label>
            <input type="text" name="link" id="link" required>
            <span id="embed-tooltip">?<span id="embed-tooltip-text">Quand vous avez ouvert une vidéo youtube : appuyez sur "Partager" -> "Intégrer" -> Copiez le code commençant par "&ltiframe"</span></span>
        </div>
        <input type="submit" value="Envoyer" class="submit">
    </fieldset>
</form>

<h2>Editer le rôle d'un utilisateur</h2>
<form action="administration" method="POST" class="form2">
    <fieldset>
        <div class="form-div">  
            <label for="user-update">Utilisateur</label>
            <select name="user-update" id="user">
                <option value="">-- Choisir un utilisateur --</option>
                <?php
                    $userDAO = new UserDAO;
                    $userDAO->findAll();
                    $users = User::getUserList();
                    foreach ($users as $user){
                        echo '<option value="' . $user->getId() . '">' . $user->getUsername() . '</option>';
                    }
                ?>
            </select>
        </div>
        <div class="form-div">
            <label for="role-update">Rôle</label>
            <select name="role-update" id="role-update">
                <option value="">-- Choisir un rôle --</option>
                <?php
                    $roleDAO = new RoleDAO;
                    $roleDAO->findAll();
                    $roles = Role::getRoleList();
                    foreach ($roles as $role){
                        echo '<option value="' . $role->getId() . '">' . $role->getRole_label() . '</option>';
                    }
                ?>
            </select>
        </div>
        <input type="submit" value="Envoyer" class="submit">
    </fieldset>
</form>

<h2>Créer un nouveau rôle</h2>
<form action="administration" method="POST" class="form1">
    <fieldset>
        <div class="form-div">
            <label for="role-label">Nom du rôle</label>
            <input type="text" name="role-label" id="role-label">
        </div>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="form-div-grid">
            <?php
                $permissionDAO = new PermissionDAO;
                $permissionDAO->findAll();
                $permissions = Permission::getPermissionList();
                foreach ($permissions as $permission){
                    echo '<div class="checkbox"><label for="' . $permission->getLabel() . '">' . $permission->getLabel();
                    echo '<input type="checkbox" name="' . $permission->getLabel() . '" value="' . $permission->getId() . '"></div>';
                }
            ?>
        </div>
    </fieldset>
    <input type="submit" value="Envoyer" class="submit">
</form>

<h2>Ajouter une chaîne YouTube</h2>
<form action="administration" method="POST" class="form2">
    <fieldset>
        <div class="form-div">
            <label for="new-channel">Nom de la chaîne</label>
            <input type="text" name="new-channel" id="new-channel">
        </div>
        <input type="submit" value="Envoyer" class="submit">
    </fieldset>
</form>
<?php
require_once(realpath("../src/view/common/footer.php"));
?>