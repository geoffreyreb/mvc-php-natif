<footer>
    <div class="footer">
        <a href="#hautDePage" title="Retour en haut" class="footer-link">Retour en haut</a>
        <a href="contact" title="Nous contacter" class="footer-link">Nous contacter</a>
        <a href="cgu" title="Conditions générales d'utilisation" class="footer-link">Conditions Générales
            d'utilisation</a>
        <a href="cgu" title="Politique de confidentialité" class="footer-link">Politique de confidentialité</a>
        <a href="cgu" title="Mentions légales" class="footer-link">Mentions légales</a>
    </div>
    <p class="credential">© 2022 - Geoffrey Rebmann / Thibaut Grunewald</p>
</footer>

<script src="./assets/script/common.js"></script>
</body>