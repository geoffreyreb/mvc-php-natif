<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="
            Bienvenue sur Global financièrement Votre. Le but de notre site est de démocratiser le marketing relationnel.
        ">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Global Financièrement Votre</title>

        <link rel="stylesheet" href="./assets/css/style.css">
        <?php
            $page = "accueil";
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }
        ?>

        <link rel="stylesheet" href="./assets/css/<?= $page ?>.css">
    </head>
