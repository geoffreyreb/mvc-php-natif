<?php
use App\DAO\UserDAO;
$userDAO = new UserDAO;
?>
<body>
    <header id="hautDePage">
        <div id="logo-div">
            <a href="accueil" title="Retour à la page d'accueil"><img src="./assets/images/logo.png" alt="Global financièrement Votre" id="logo"></a>
        </div>
        <div id="nav-bar" class="hidden">
            <ul>
                <li id="accueil">
                    <a class="link" href="accueil" title="Retour à la page d'accueil">Accueil</a>
                </li>
                <li id="emission">
                    <a class="link" href="emission" title="Découvrez nos vidéos">Mon émission</a>
                </li>
                <li id="partners">
                    <a class="link" href="partners" title="Découvrez les sociétés avec qui nous travaillons">Sociétés Partenaires</a>
                </li>
                <li id="packages">
                    <a class="link" href="packages" title="Découvrez nos offres">Créer son Marketing relationnel</a>
                </li>
                <li id="contact">
                    <a class="link" href="contact" title="Contactez nous">Contact</a>
                </li>
                <?php 
                        if (!isset($_SESSION['user'])){
                            ?>
                            <li id="login">
                                <a class="link" href="login" title="Page de connexion">Se connecter</a>
                            </li>
                            <?php
                        }
                        else{
                            ?>
                            <li>
                                <a class="link" href="logout" title="Déconnexion">Se déconnecter</a>
                            </li>
                            <li id="logged-as">
                                Connecté en tant que  
                                <?php
                                $user = $userDAO->findBy("username", "'" . $_SESSION['user']->getUsername() . "'");
                                $permissions = $userDAO->findPermissions($user);
                                if (isset($permissions[1]) && $permissions[1] == "MANAGE_SITE"){
                                    echo '<a id="admin" href="administration" title="Cliquez ici pour administrer le site">';
                                    echo $_SESSION['user']->getUsername();
                                    echo "</a>";
                                }
                                else{
                                    echo $_SESSION['user']->getUsername();
                                }
                                ?>
                            </li>
                            <?php
                        }
                    ?>

            </ul>
        </div>
        <div id="menu-burger">Menu</div>
    </header>
</body>