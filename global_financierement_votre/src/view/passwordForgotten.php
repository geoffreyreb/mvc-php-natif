<?php

use App\Utilities\Security;
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
?>

    <form action="passwordForgotten" method="POST">
    <?php
        if (!isset($_SESSION['user-forgotaccount'])){
    ?>
        <fieldset>
            <legend>Récupation du mot de passe</legend>
            <label for="mail">Votre adresse mail :</label>
            <input type="email" name="mail" id="mail" required>
            
        </fieldset>
        <?php
            }else{
        ?>
        <fieldset>
            <legend>Réécriture du mot de passe</legend>
            <label for="password">Nouveau mot de passe :</label>
            <input type="password" name="password" id="password">
        </fieldset>
        <?php
            }
        ?>
        <input type="hidden" name="<?=Security::CSRF_TOKEN?>" value="<?=Security::generateCSRFToken()?>">
        <input type="submit" value="Valider" class='button'>
    </form>
</body>
<?php
require_once(realpath("../src/view/common/footer.php"));
?>