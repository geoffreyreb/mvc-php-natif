<?php
require_once(realpath("../src/view/common/head.php"));
require_once(realpath("../src/view/common/header.php"));
use App\Utilities\Security;
?>
    <form action="register" method="post">
        <fieldset>
            <legend>S'inscrire</legend>
            <div>
                <label for="username">Nom d'utilisateur</label>
                <input type="text" name="username" id="username" required>
            </div>
            <div>
                <label for="mail">E-mail</label>
                <input type="email" name="mail" id="mail" required>
            </div>
            <div>
                <label for="pswrd">Mot de passe</label>
                <input type="password" name="pswrd" id="pswrd" required>
            </div>
            <div>
                <label for="firstname">Prénom</label>
                <input type="text" name="firstname" id="firstname" required>
            </div>
            <div>
                <label for="lastname">Nom</label>
                <input type="text" name="lastname" id="lastname" required>
            </div>
            <div>
                <label for="address">Adresse</label>
                <input type="text" name="address" id="address" required>
            </div>
            <input type="submit" value="S'enregistrer" class='submit'>
            <input type="hidden" name="<?= Security::CSRF_TOKEN ?>" value="<?= Security::generateCSRFToken() ?>">
        </fieldset>
    </form>
    <p>
        <a href="login" title="Se connecter" id="login">Vous avez déjà un compte ? Connectez-vous.</a>
    </p>
<?php
require_once(realpath("../src/view/common/footer.php"));
?>