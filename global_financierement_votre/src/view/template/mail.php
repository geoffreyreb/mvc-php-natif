<?php
require_once(realpath("../view/common/head.php"));
?>

    <main>
        <h1>Bonjour {{name}},</h1>
        <p> Merci de votre confiance. <br>
            Vous venez de m'indiquer que vous avez besoin d'un nouveau mot de passe.
        </p>
        <p>Pour le réinitialiser, cliquez ici :</p>
        <a href="http://localhost:8809/passwordForgotten?token={{token}}" title="Réinitialiser le mot de passe">Rénitinialiser votre mot de passe</a>
        <p>ATTENTION : ce lien sera obsolète au bout de 30 minutes.</p>
    </main>
