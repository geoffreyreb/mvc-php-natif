drop schema if exists app;
create schema if not exists app;
use app;

CREATE TABLE `User`(
   id_user SERIAL,
   username VARCHAR(50)  NOT NULL,
   user_firstname VARCHAR(250)  NOT NULL,
   user_lastname VARCHAR(250)  NOT NULL,
   user_mail VARCHAR(250)  NOT NULL,
   user_address VARCHAR(500)  NOT NULL,
   user_password VARCHAR(250)  NOT NULL,
   user_authentication_token VARCHAR(250) ,
   user_authentication_lifespan DATETIME,
   user_reset_token VARCHAR(250) ,
   user_reset_expiration VARCHAR(50) ,
   PRIMARY KEY(id_user),
   UNIQUE(username)
);

CREATE TABLE Role(
   id_role SERIAL,
   role_label VARCHAR(250)  NOT NULL,
   PRIMARY KEY(id_role)
);

CREATE TABLE Permission(
   id_permission SERIAL,
   permission_label VARCHAR(250)  NOT NULL,
   PRIMARY KEY(id_permission)
);

CREATE TABLE Article(
   id_article SERIAL,
   article_title VARCHAR(250)  NOT NULL,
   article_content TEXT NOT NULL,
   article_summary VARCHAR(250)  NOT NULL,
   article_image_path VARCHAR(250) ,
   article_image_description VARCHAR(250) ,
   article_date DATETIME NOT NULL,
   id_user BIGINT UNSIGNED NOT NULL NOT NULL,
   PRIMARY KEY(id_article),
   FOREIGN KEY(id_user) REFERENCES `User`(id_user)
);

CREATE TABLE Package(
   id_package SERIAL,
   package_name VARCHAR(50)  NOT NULL,
   id_role BIGINT UNSIGNED NOT NULL NOT NULL,
   PRIMARY KEY(id_package)
);

CREATE TABLE Channel(
   id_channel SERIAL,
   channel_name VARCHAR(250)  NOT NULL,
   PRIMARY KEY(id_channel)
);

CREATE TABLE Video(
   id_video SERIAL,
   video_title VARCHAR(250)  NOT NULL,
   video_link VARCHAR(500)  NOT NULL,
   video_upload_date DATETIME NOT NULL,
   id_channel BIGINT UNSIGNED NOT NULL NOT NULL,
   PRIMARY KEY(id_video),
   FOREIGN KEY(id_channel) REFERENCES Channel(id_channel)
);

CREATE TABLE ACL(
   id_role BIGINT UNSIGNED NOT NULL,
   id_permission BIGINT UNSIGNED NOT NULL,
   PRIMARY KEY(id_role, id_permission),
   FOREIGN KEY(id_role) REFERENCES Role(id_role),
   FOREIGN KEY(id_permission) REFERENCES Permission(id_permission)
);

CREATE TABLE Role_by_user(
   id_user BIGINT UNSIGNED NOT NULL,
   id_role BIGINT UNSIGNED NOT NULL,
   PRIMARY KEY(id_user, id_role),
   FOREIGN KEY(id_user) REFERENCES `User`(id_user),
   FOREIGN KEY(id_role) REFERENCES Role(id_role)
);

CREATE TABLE Package_by_user(
   id_user BIGINT UNSIGNED NOT NULL,
   id_package BIGINT UNSIGNED NOT NULL,
   PRIMARY KEY(id_user, id_package),
   FOREIGN KEY(id_user) REFERENCES `User`(id_user),
   FOREIGN KEY(id_package) REFERENCES Package(id_package)
);

insert into Role (role_label) values ("Admin");
insert into Role (role_label) values ("Guest");

insert into Permission (permission_label) values ("MANAGE_SITE");
insert into Permission (permission_label) values ("CREATE_USER");
insert into Permission (permission_label) values ("READ_USER");
insert into Permission (permission_label) values ("UPDATE_USER");
insert into Permission (permission_label) values ("DELETE_USER");
insert into Permission (permission_label) values ("CREATE_ROLE");
insert into Permission (permission_label) values ("READ_ROLE");
insert into Permission (permission_label) values ("UPDATE_ROLE");
insert into Permission (permission_label) values ("DELETE_ROLE");
insert into Permission (permission_label) values ("CREATE_ARTICLE");
insert into Permission (permission_label) values ("READ_ARTICLE");
insert into Permission (permission_label) values ("UPDATE_ARTICLE");
insert into Permission (permission_label) values ("DELETE_ARTICLE");
insert into Permission (permission_label) values ("CREATE_PACKAGE");
insert into Permission (permission_label) values ("READ_PACKAGE");
insert into Permission (permission_label) values ("UPDATE_PACKAGE");
insert into Permission (permission_label) values ("DELETE_PACKAGE");
insert into Permission (permission_label) values ("CREATE_CHANNEL");
insert into Permission (permission_label) values ("READ_CHANNEL");
insert into Permission (permission_label) values ("UPDATE_CHANNEL");
insert into Permission (permission_label) values ("DELETE_CHANNEL");
insert into Permission (permission_label) values ("VIEW_SITE");

insert into ACL (id_role, id_permission) values (1, 1);
insert into ACL (id_role, id_permission) values (1, 2);
insert into ACL (id_role, id_permission) values (1, 3);
insert into ACL (id_role, id_permission) values (1, 4);
insert into ACL (id_role, id_permission) values (1, 5);
insert into ACL (id_role, id_permission) values (1, 6);
insert into ACL (id_role, id_permission) values (1, 7);
insert into ACL (id_role, id_permission) values (1, 8);
insert into ACL (id_role, id_permission) values (1, 9);
insert into ACL (id_role, id_permission) values (1, 10);
insert into ACL (id_role, id_permission) values (1, 11);
insert into ACL (id_role, id_permission) values (1, 12);
insert into ACL (id_role, id_permission) values (1, 13);
insert into ACL (id_role, id_permission) values (1, 14);
insert into ACL (id_role, id_permission) values (1, 15);
insert into ACL (id_role, id_permission) values (1, 16);
insert into ACL (id_role, id_permission) values (1, 17);
insert into ACL (id_role, id_permission) values (1, 18);
insert into ACL (id_role, id_permission) values (1, 19);
insert into ACL (id_role, id_permission) values (1, 20);
insert into ACL (id_role, id_permission) values (1, 21);
insert into ACL (id_role, id_permission) values (1, 22);
insert into ACL (id_role, id_permission) values (2, 22);